# timetools #

This is a simple python module with some handy functions to work with time and date in python.

## Installation ##

Install the module by downloading the .zip file under /dist/ and execute 

`pip install ./timetools-X.x.zip`

Or you can simply copy the timetools.py file to your project folder. 

## Usage ##

