#!/usr/bin/env python
# -*- encoding: utf-8 -*-

from setuptools import setup

with open('README.md') as f:
	readme = f.read()

setup(
	name='timetools',
	version='1.0',
	description='A small python module to make working with time and date a bit easier',
	url='https://bitbucket.org/Alexander_Knoop/timetools',
	author='Alexander Knoop',
	author_email='computer.online@web.de',
	py_modules=['timetools'],
	license='GPL-3.0',
	long_description=readme,
	)
